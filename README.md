<!-- language: lang-none -->

    .  . .-. .-. .-. .   . . .-. 
    |\/| | | |  )|-| |    |  `-. 
    '  ` `-' `-' ` ' `-'  `  `-' 

Welcome to Modalys beta git repository!
==============================
This is is the place for experimental and beta releases of Modalys.

Please feel free to create any "issue" for bug reports ;-)
